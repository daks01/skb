// calculator
function CartForm($scope) {
    $scope.invoice = {
        items: [{cost: 11782},{cost: 3678},{cost: 678},{cost: -5423}]
    };
    $scope.addItem = function() {
        $scope.invoice.items.push({cost: 0, edit: 'edit'});
    },
    $scope.removeItem = function(index) {
        $scope.invoice.items.splice(index, 1);
    },
    $scope.total = function() {
        var total = 0;
        angular.forEach($scope.invoice.items, function(item) {
            total += item.cost*1;
        })
        return total;
    }
}


// fix total price at bottom
function fixTotalPrice() {
    var table = jQuery('.ng-scope'),
         tableH = jQuery('.ng-scope').outerHeight(true),
         fixDiv = jQuery('.total'),
         fixDivH = jQuery('.total').outerHeight(true),
         documentH = jQuery(document).height(),
         windowH = jQuery(window).height(),
         scrolPos = jQuery(window).scrollTop();

    checkScroll(scrolPos);

    jQuery(window).scroll(function(){
        scrolPos = jQuery(window).scrollTop();
        checkScroll(scrolPos);
    });

    function checkScroll(scrolPos){
        if(tableH > windowH){
            if(scrolPos+windowH < documentH){
                fixDiv.addClass('fixed');
            }else{
                fixDiv.removeClass('fixed');
            }
        }else{
            fixDiv.removeClass('fixed');
        }
    }
}
jQuery(document).ready(function(){
    fixTotalPrice();
});

jQuery(window).resize(function(){
    fixTotalPrice();
});

var height = jQuery(document).height();
jQuery(document).bind('DOMSubtreeModified', function() {
    if(jQuery(this).height() != height) {
        fixTotalPrice();
    }
});
